import { useEffect, useState } from "react";
import { BsCheck2Circle } from "react-icons/bs";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { InputText } from "primereact/inputtext";
import { Calendar } from "primereact/calendar";
import moment from "moment/moment";
import { API_URL } from "../../variables";
import axios from "axios";

function Home() {
  const [searchValue, setSearchValue] = useState("");
  const [date, setDate] = useState(new Date());
  const [listAttendance, setListAttendance] = useState([]);

  const prevDate = () => {
    setDate(moment(date).subtract(1, "days")._d);
    setSearchValue("");
  };

  const nextDate = () => {
    setDate(moment(date).add(1, "days")._d);
    setSearchValue("");
  };

  const dateBodyTemplate = (rowData) => {
    return moment(rowData.attendance_time).format("HH:mm DD/MM/YYYY");
  };

  const getDataAttendance = async () => {
    try {
      const response = await axios.get(`${API_URL}/student-attendance`, {
        params: {
          from: moment(date).format("YYYY-MM-DD"),
          to: moment(date).format("YYYY-MM-DD"),
        },
      });
      console.log(response);
      if (response.data.status === 200) {
        setListAttendance(response.data.data);
      }
    } catch (error) {
      if (error.response.data) return error.response.data;
      else return { success: false, message: error.message };
    }
  };
  const findByNameOrClass = () => {
    if (searchValue.trim() === "") {
      getDataAttendance();
    }
    if (listAttendance.length > 0) {
      const listByName = listAttendance.filter((item) => {
        return item.student_name.toLowerCase().includes(searchValue.toLowerCase());
      });
      if (listByName.length > 0) {
        setListAttendance(listByName);
        return;
      }
      const listByClass = listAttendance.filter((item) => {
        return item.class.toLowerCase().includes(searchValue.toLowerCase());
      });
      if (listByClass.length > 0) {
        setListAttendance(listByClass);
        return;
      }
      setListAttendance([]);
    }
  };
  const renderSTT = (rowData) => {
    return listAttendance.indexOf(rowData) + 1;
  };
  useEffect(() => {
    getDataAttendance();
  }, [date]);
  return (
    <div className="bg-[#ebedef] w-screen h-screen">
      <header className="bg-white min-h-[56px] flex items-center pl-6 border-b border-b-[#d8dbe0]">
        <div className="!bg-primary w-fit text-lg text-white py-2 px-6 rounded-full">
          Cơ sở Cát lái
        </div>
      </header>
      <div>
        <div className="bg-white min-h-[56px] flex items-center pl-6 border-b border-b-[#d8dbe0] mb-2">
          <p className="text-[#8a93a2]">Dashboard</p>
        </div>
        <div className="bg-white h-[44px] pl-3 m-[14px]">
          <div className=" border-b border-b-primary flex gap-2 w-fit">
            <BsCheck2Circle className="text-primary h-[44px]" />
            <div className="!text-primary text-base font-semibold w-fit h-full leading-[44px]">
              Điểm danh thư viện
            </div>
          </div>

          <div className="mt-[14px] bg-white pt-3">
            <div className="md:h-[44px] flex flex-col md:flex-row items-center justify-between px-3 mb-3">
              <div className="flex gap-2">
                <div className="flex">
                  <button
                    className="h-[44px] px-2 !bg-primary text-white w-[100px] rounded-l"
                    onClick={() => prevDate()}
                  >
                    <i
                      className="pi pi-arrow-left mr-2"
                      style={{ fontSize: "1.2rem" }}
                    ></i>
                    <span>Trước</span>
                  </button>
                  <button
                    className="h-[44px] px-2 !bg-primary text-white w-[100px] rounded-r"
                    onClick={() => nextDate()}
                  >
                    <span>Sau</span>
                    <i
                      className="pi pi-arrow-right ml-2"
                      style={{ fontSize: "1.2rem" }}
                    ></i>
                  </button>
                </div>
                <Calendar
                  className="h-[44px] !bg-primary rounded-sm"
                  value={date}
                  onChange={(e) => {
                    setDate(e.value);
                    setSearchValue("");
                  }}
                  showIcon
                  showButtonBar
                  dateFormat="dd/mm/yy"
                />
              </div>
              <div className="flex font-semibold">
                <p className="text-primary mr-1">
                  Tổng Học sinh vào Thư viện:{" "}
                </p>
                <p className="">{listAttendance.length}</p>
              </div>
              <div className="flex">
                <InputText
                  className="h-[44px]"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  placeholder="Tên học sinh hoặc tên lớp"
                />
                <button
                  className="w-[44px] h-[44px] !bg-primary text-white rounded"
                  onClick={() => findByNameOrClass()}
                >
                  <span className="pi pi-search"></span>
                </button>
              </div>
            </div>
            <DataTable
              scrollable
              scrollHeight="800px"
              emptyMessage="Không tìm thấy học sinh."
              paginator
              rows={10}
              rowsPerPageOptions={[10, 25, 50]}
              value={listAttendance}
              tableStyle={{ minWidth: "100%" }}
            >
              <Column
                field="code"
                header="STT"
                style={{ width: "10%" }}
                body={renderSTT}
              ></Column>
              <Column
                field="student_name"
                header="Họ và tên"
                style={{ width: "40%" }}
              ></Column>
              <Column
                field="class"
                header="Lớp"
                style={{ width: "15%" }}
              ></Column>
              <Column
                field="attendance_time"
                body={dateBodyTemplate}
                header="Thời gian điểm danh"
                style={{ width: "35%" }}
              ></Column>
            </DataTable>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
